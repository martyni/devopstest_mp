class devops {

  package { 'httpd':
    ensure => 'installed',
    
  }

  service { 'httpd':
        ensure => running,
        require => Package['httpd'],
  }

  file { '/var/www/devopstest':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0744',
    require => [Package['httpd'], File["/etc/httpd/conf.d/devops.conf"]]
  }

  file { "/etc/httpd/conf.d/devops.conf":
      mode   => 444,
      owner  => root,
      group  => root,
      content => '<VirtualHost *:80>
   DocumentRoot "/var/www/devopstest"
   ServerName web.devops.test
</VirtualHost>',
      require => Package['httpd']
  }

  exec { 'create_needed_directory':
    command => '/usr/bin/curl https://s3-eu-west-1.amazonaws.com/kaplandevopstest/version.json>/var/www/devopstest/index.html',
    require => File['/var/www/devopstest'],
  }

  exec { 'restart apache':
    command => '/usr/sbin/service httpd restart',
    require => File["/etc/httpd/conf.d/devops.conf"],
  }
}
